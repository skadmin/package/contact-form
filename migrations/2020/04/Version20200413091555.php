<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200413091555 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'contact-form.overview', 'hash' => 'ce59b2fdab49409b3d83dbeb9411eb45', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kontaktní formulář', 'plural1' => '', 'plural2' => ''],
            ['original' => 'contact-form.overview.title', 'hash' => 'c2a1c31c7e3052ba6a60afae2af9996b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kontaktní formulář|Přehled příchozích zpráv', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.status.unarchive', 'hash' => '1f37e3ade640e89fd6cd685cf534c44c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'ne', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.status.archive', 'hash' => 'cbb0b437e1034ac56aa6014120b5a19f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'ano', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.is-active', 'hash' => '8a6ce7c3e61487625b656bc6f2f33961', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je archivovaný?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.name', 'hash' => '3d773787b16132c0d6c134a4bd855387', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odesilatel', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.created-at', 'hash' => 'f22bf7a7065e49d480a06f10e228f64d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odesláno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.action.archive', 'hash' => '4dd9e13ee0b26252c2ab0fe8f9b56190', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.action.archive.title', 'hash' => '22ecd1c8f3cd2f49dd354c5e303e659f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Archivovat', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.action.unarchive', 'hash' => '5802d5d0e2acb3ab9a6ab8b5a4f8b9d5', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.action.unarchive.title', 'hash' => '946c79e461052e23ae1b4fa1a85853b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obnovit z archívu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.contact', 'hash' => 'ca95a77f527953d00f7501606bc234bb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kontakt', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.contact-form.title', 'hash' => 'e813247c06a7ee993173dfcaa6694f26', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kontaktní formulář', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.contact-form.description', 'hash' => '914ed1a78322ac8a0ff276b23ec7b080', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje zobrazit dotazy z webu', 'plural1' => '', 'plural2' => ''],
            // mail
            ['original' => 'mail.contact-form-create.name', 'hash' => '65f548174950f0f71d1415295a1821d4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Kontaktní formulář', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.title.description', 'hash' => 'ce8f7fb02eeeb30b1ad218ef864633e9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Předmět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.title.example', 'hash' => '62c7ce6e6030db60642278db85472382', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Váš předmět je: [title] =&gt; <strong>Váš předmět je: Neplatný odkaz</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.content.description', 'hash' => 'b4234c5538067b60850776dce8a23dfb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.content.example', 'hash' => 'be28c15324d0d1217fa0601cfe19ec22', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah zprávy: [content] =&gt; <strong>Obsah zprávy: Dobrý ..., děkuji, Suchá</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.name.description', 'hash' => 'fa90c5c5f1078460a4e17f8ca0008d46', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.name.example', 'hash' => '19d1b4e279507a1b592ac754f925a107', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno: [name] =&gt; <strong>Jméno: Alzběta Suchá</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.vocativ-name.description', 'hash' => '1d2cb02da1f2d3472a20ca301e94d19c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Skloňované jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.vocativ-name.example', 'hash' => '92972744cbcd3f2a8ffe4875386e21e1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dobrý den, [vocativ-name] =&gt; <strong>Dobrý den, Alžběto Suchá</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.email.description', 'hash' => '755cd1135bc2aa0737bfef12f0081778', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.email.example', 'hash' => 'bdad57f888e757fd08f43f3db0fddf58', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail: [email] =&gt; <strong>E-mail: alzbeta.sucha@web.it</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.phone.description', 'hash' => 'ef54902ae65a2e83c74f1e6f9993acae', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Telefon', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.phone.example', 'hash' => '74d7720b9339b5e0fc737086a0f42824', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Telefon: [phone] =&gt; <strong>Telefon: +420 123 456 789</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.created-at.description', 'hash' => 'a03d654831d0374caf0786c6470602b6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odesláno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form.parameter.created-at.example', 'hash' => '023ec66505a62079601f831ab9e6c0a0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail by odeslán [created-at] =&gt; <strong>E-mail by odeslán 10.12.1998</strong>', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
