<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200816050656 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $oldTranslations = [
            ['hash' => 'ce8f7fb02eeeb30b1ad218ef864633e9'],
            ['hash' => '62c7ce6e6030db60642278db85472382'],
            ['hash' => 'b4234c5538067b60850776dce8a23dfb'],
            ['hash' => 'be28c15324d0d1217fa0601cfe19ec22'],
            ['hash' => 'fa90c5c5f1078460a4e17f8ca0008d46'],
            ['hash' => '19d1b4e279507a1b592ac754f925a107'],
            ['hash' => '1d2cb02da1f2d3472a20ca301e94d19c'],
            ['hash' => '92972744cbcd3f2a8ffe4875386e21e1'],
            ['hash' => '755cd1135bc2aa0737bfef12f0081778'],
            ['hash' => 'bdad57f888e757fd08f43f3db0fddf58'],
            ['hash' => 'ef54902ae65a2e83c74f1e6f9993acae'],
            ['hash' => '74d7720b9339b5e0fc737086a0f42824'],
            ['hash' => 'a03d654831d0374caf0786c6470602b6'],
            ['hash' => '023ec66505a62079601f831ab9e6c0a0'],
        ];

        foreach ($oldTranslations as $oldTranslation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $oldTranslation);
        }

        $translations = [
            ['original' => 'grid.contact-form.overview.email-to', 'hash' => '75ed148e309180beb0c9fa73d64b8d97', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Příjemce', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.files-count', 'hash' => '45b742ee87e093a6d2dc406ee233bf67', 'module' => 'admin', 'language_id' => 1, 'singular' => '	grid.contact-form.overview.files-count', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.contact-form.overview.files-count %s', 'hash' => '32fc723ef8c7af59f858313151ba994d', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s příloha', 'plural1' => '%s přílohy', 'plural2' => '%s příloh'],
            ['original' => 'grid.contact-form.overview.files', 'hash' => '4926b8d1ce39dfb381401315335613d0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přílohy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.title.description', 'hash' => 'e3e5877c3b9a2ba919e92cee195e8549', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Předmět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.title.example', 'hash' => '4aa0430edb777c536dadc95b290b771a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Váš předmět je: [title] =&gt; <strong>Váš předmět je: Neplatný odkaz</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.content.description', 'hash' => 'd4b2e8e583538ad46eaa607cb1c507a3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.content.example', 'hash' => '0019074b94718bd9956ead3d9ef551bd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah zprávy: [content] =&gt; <strong>Obsah zprávy: Dobrý ..., děkuji, Suchá</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.name.description', 'hash' => 'a9874d9d4beb707714170886758260d7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.name.example', 'hash' => 'b52ed66a03c0f25df503d52d9b17c45d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jméno: [name] =&gt; <strong>Jméno: Alzběta Suchá</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.vocativ-name.description', 'hash' => 'd1e4d7fb9ffce98eb80b7869a54ddf3a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Skloňované jméno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.vocativ-name.example', 'hash' => '359866357bc5cef45e89806ea1826a63', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Dobrý den, [vocativ-name] =&gt; <strong>Dobrý den, Alžběto Suchá</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.email.description', 'hash' => '668cbbd99fc1c3de946acf2581c87732', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.email.example', 'hash' => '283b8f0c59eb0919232def10d4e55643', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail: [email] =&gt; <strong>E-mail: alzbeta.sucha@web.it</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.phone.description', 'hash' => '4147df4efdac995775de308b6f03c7bf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Telefon', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.phone.example', 'hash' => '781353eb2424143387ab6714fdc9e0a0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Telefon: [phone] => <strong>Telefon: +420 123 456 789</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.email-to.description', 'hash' => '422cbdf2334346b28ce402508d77100a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Příjemce', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.email-to.example', 'hash' => '41ebbc6141357684358834c3e0a36002', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Příjemce: [email-to] => <strong>Příjemce: prijemce@skadmin.cz</strong>', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.attachments.description', 'hash' => '8e253bfe9f33f5d6b34e1ead6c026968', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přílohy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.attachments.example', 'hash' => '3e743bbc245ace7941a81beb7f1fb616', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Automaticky se přidávají ke zprávě', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.created-at.description', 'hash' => 'b776c7505452584e9adb801da125555c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odesláno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mail.contact-form-create.parameter.created-at.example', 'hash' => 'f912463befa58dd5cda0a10f2130f782', 'module' => 'admin', 'language_id' => 1, 'singular' => 'E-mail by odeslán [created-at] =&gt; <strong>E-mail by odeslán 10.12.1998</strong>', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
