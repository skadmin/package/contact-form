<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\ContactForm\Mail\CMailContactFormCreate;

use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200815050843 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // update email template
        $mailTemplatesUpdate = [
            [
                'parameters'         => serialize(CMailContactFormCreate::getModelForSerialize()),
                'type'               => CMailContactFormCreate::TYPE,
                'last_update_author' => 'Cron',
                'last_update_at'     => (new DateTime())->format('Y-m-d H:i:s'),
                'recipients'         => '',
                'preheader'          => '',

            ],
        ];

        foreach ($mailTemplatesUpdate as $mailTemplate) {
            $this->addSql('UPDATE mail_template SET parameters = :parameters, last_update_author = :last_update_author, last_update_at = :last_update_at WHERE type = :type', $mailTemplate);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
