<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\ContactForm\BaseControl;
use Skadmin\ContactForm\Mail\CMailContactFormCreate;

use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200318131526 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'contact-form',
            'name'         => 'Contact form',
            'is_active'    => 1,
        ];

        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resources = [
            [
                'name'        => 'contact-form',
                'title'       => 'role-resource.contact-form.title',
                'description' => 'role-resource.contact-form.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }

        $mailTemplates = [
            [
                'content'            => '',
                'parameters'         => serialize(CMailContactFormCreate::getModelForSerialize()),
                'type'               => CMailContactFormCreate::TYPE,
                'class'              => CMailContactFormCreate::class,
                'name'               => 'mail.contact-form-create.name',
                'subject'            => 'mail.contact-form-create.subject',
                'last_update_author' => 'Cron',
                'last_update_at'     => (new DateTime())->format('Y-m-d H:i:s'),
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql(
                'INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)',
                $mailTemplate
            );
        }
    }

    public function down(Schema $schema): void
    {
    }
}
