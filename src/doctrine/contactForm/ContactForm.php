<?php

declare(strict_types=1);

namespace Skadmin\ContactForm\Doctrine\ContactForm;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Validators;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ContactForm
{
    use Entity\Id;
    use Entity\Title;
    use Entity\Content;
    use Entity\Name;
    use Entity\Contact;
    use Entity\Created;
    use Entity\IsActive;

    /** @var string[] */
    #[ORM\Column(type: Types::ARRAY)]
    private array $files = [];

    #[ORM\Column]
    private string $emailTo = '';

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * @param string[] $files
     */
    public function create(string $title, string $content, string $name, string $email, string $phone, string $emailTo, array $files): void
    {
        $this->title   = $title;
        $this->content = $content;
        $this->name    = $name;
        $this->phone   = $phone;
        $this->emailTo = $emailTo;

        $this->files = $files;

        $this->setEmail($email);
    }

    public function getEmailTo(): string
    {
        return $this->emailTo;
    }

    public function setEmailTo(string $emailTo): void
    {
        $this->emailTo = Validators::isEmail($emailTo) ? $emailTo : '';
    }

    /**
     * @return string[]
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    public function archive(): void
    {
        $this->isActive = false;
    }

    public function unarchive(): void
    {
        $this->isActive = true;
    }
}
