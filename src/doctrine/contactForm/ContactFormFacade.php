<?php

declare(strict_types=1);

namespace Skadmin\ContactForm\Doctrine\ContactForm;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class ContactFormFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = ContactForm::class;
    }

    /**
     * @param string[]|array $files
     */
    public function create(string $title, string $content, string $name, string $email, string $phone, string $emailTo = '', array $files = []): ContactForm
    {
        $contactForm = $this->get();

        $contactForm->create($title, $content, $name, $email, $phone, $emailTo, $files);

        $this->em->persist($contactForm);
        $this->em->flush();

        return $contactForm;
    }

    public function get(?int $id = null): ContactForm
    {
        if ($id === null) {
            return new ContactForm();
        }

        $contactForm = parent::get($id);

        if ($contactForm === null) {
            return new ContactForm();
        }

        return $contactForm;
    }

    public function archive(ContactForm $contactForm): ContactForm
    {
        $contactForm->archive();

        $this->em->persist($contactForm);
        $this->em->flush();

        return $contactForm;
    }

    public function unarchive(ContactForm $contactForm): ContactForm
    {
        $contactForm->unarchive();

        $this->em->persist($contactForm);
        $this->em->flush();

        return $contactForm;
    }
}
