<?php

declare(strict_types=1);

namespace Skadmin\ContactForm\Components\Front;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Http\FileUpload;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Validators;
use Skadmin\ContactForm\BaseControl;
use Skadmin\ContactForm\Doctrine\ContactForm\ContactFormFacade;
use Skadmin\ContactForm\Mail\CMailContactFormCreate;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Model\MailService;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function assert;

class FormContactForm extends FormWithUserControl
{
    use APackageControl;

    /** @var callable[] */
    public array              $onValidate;
    private ContactFormFacade $facade;
    private LoaderFactory     $webLoader;
    private FileStorage       $fileStorage;
    private MailService       $mailService;

    public function __construct(ContactFormFacade $facade, Translator $translator, LoaderFactory $webLoader, FileStorage $fileStorage, MailService $mailService, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade      = $facade;
        $this->webLoader   = $webLoader;
        $this->fileStorage = $fileStorage;
        $this->mailService = $mailService;
    }

    public function getTitle(): string
    {
        return 'form.contact-form.front.create.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [$this->webLoader->createCssLoader('customFileInput')];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('reCaptchaInvisible'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
        ];
    }

    public function proccessOnValidate(Form $form, ArrayHash $values): void
    {
        $this->onValidate($form, $values);
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $files = [];
        foreach ($values->files as $file) {
            assert($file instanceof FileUpload);
            if (! $file->isOk()) {
                continue;
            }

            $files[] = $this->fileStorage->save($file, BaseControl::DIR_FILE);
        }

        $contactForm = $this->facade->create($values->title, $values->content, $values->name, $values->email, $values->phone, $values->emailTo, $files);

        $cMailContactFormCreate = new CMailContactFormCreate($contactForm);

        $recipients = [$values->email];
        if (Validators::isEmail($values->emailTo)) {
            $recipients[] = $values->emailTo;
        }

        $mailQueue = $this->mailService->addByTemplateType(
            CMailContactFormCreate::TYPE,
            $cMailContactFormCreate,
            $recipients,
            true
        );

        if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $this->onFlashmessage('form.contact-form.front.create.flash.success-mail', Flash::SUCCESS);
        } else {
            $this->onFlashmessage('form.contact-form.front.create.flash.danger', Flash::DANGER);
        }

        $this->onSuccess($form, $values, 'send');

        $form->reset();
        $this->redrawControl('snipForm');
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/formContactForm.latte'));

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('title', 'form.contact-form.front.create.title');
        $form->addText('name', 'form.contact-form.front.create.name')
            ->setRequired('form.contact-form.front.create.name.req');
        $form->addEmail('email', 'form.contact-form.front.create.email')
            ->setRequired('form.contact-form.front.create.email.req');
        $form->addText('phone', 'form.contact-form.front.create.phone');
        $form->addTextArea('content', 'form.contact-form.front.create.content', null, 10)
            ->setRequired('form.contact-form.front.create.content.req');

        // Additional
        $form->addMultiUpload('files', 'form.contact-form.front.create.files');
        $form->addHidden('emailTo');

        // CAPTCHA
        $form->addInvisibleReCaptchaInput();

        // BUTTON
        $form->addSubmit('send', 'form.contact-form.front.create.send');

        $this->onModifyForm($form);

        // CALLBACK
        $form->onValidate[] = [$this, 'proccessOnValidate'];
        $form->onSuccess[]  = [$this, 'processOnSuccess'];

        return $form;
    }
}
