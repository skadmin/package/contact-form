<?php

declare(strict_types=1);

namespace Skadmin\ContactForm\Components\Front;

interface IFormContactFormFactory
{
    public function create(): FormContactForm;
}
