<?php

declare(strict_types=1);

namespace Skadmin\ContactForm\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
