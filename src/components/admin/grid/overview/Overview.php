<?php

declare(strict_types=1);

namespace Skadmin\ContactForm\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Utils;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\ContactForm\BaseControl;
use Skadmin\ContactForm\Doctrine\ContactForm\ContactForm;
use Skadmin\ContactForm\Doctrine\ContactForm\ContactFormFacade;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function count;
use function implode;
use function intval;

class Overview extends GridControl
{
    use APackageControl;

    public const ARCHIVE   = 0;
    public const UNARCHIVE = 1;

    public const STATUS_ARCHIVE = [
        self::UNARCHIVE => 'grid.contact-form.overview.status.unarchive',
        self::ARCHIVE   => 'grid.contact-form.overview.status.archive',
    ];

    private ContactFormFacade $facade;
    private FileStorage       $fileStorage;

    public function __construct(ContactFormFacade $facade, FileStorage $fileStorage, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->fileStorage = $fileStorage;
        $this->facade      = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->drawBox = $this->drawBox;
        $template->render();
    }

    public function getTitle(): string
    {
        return 'contact-form.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA
        $dataArchive = Arrays::map(self::STATUS_ARCHIVE, function ($item): string {
            return $this->translator->translate($item);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.contact-form.overview.name')
            ->setRenderer(function (ContactForm $contactForm): Html {
                $result = new Html();
                $result->setText($contactForm->getName());

                $filesCount = count($contactForm->getFiles());
                if ($filesCount > 0) {
                    $filesCountText = $this->translator->translate(new SimpleTranslation('grid.contact-form.overview.files-count %s', $filesCount, $filesCount));
                    $filesCountHtml = Html::el('code', ['class' => 'text-muted small'])
                        ->setText($filesCountText);

                    $result->addHtml('<br>')
                        ->addHtml($filesCountHtml);
                }

                return $result;
            });
        $grid->addColumnText('emailTo', 'grid.contact-form.overview.email-to');
        $grid->addColumnText('contact', 'grid.contact-form.overview.contact')
            ->setRenderer(static function (ContactForm $contactForm): Html {
                $contact = new Html();

                $contacts = [];
                if ($contactForm->getEmail() !== '') {
                    $contacts[] = Utils::createHtmlContact($contactForm->getEmail());
                }

                if ($contactForm->getPhone() !== '') {
                    $contacts[] = Utils::createHtmlContact($contactForm->getPhone());
                }

                $contact->addHtml(implode('<br>', $contacts));

                return $contact;
            });
        $grid->addColumnDateTime('createdAt', 'grid.contact-form.overview.created-at')
            ->setFormat('d.m.Y H:i')
            ->setSortable();
        $grid->addColumnText('isActive', 'grid.contact-form.overview.is-active')
            ->setAlign('center')
            ->setRenderer(static function (ContactForm $contactForm) use ($dataArchive): Html {
                $status = Html::el('span');

                if ($contactForm->isActive()) {
                    $status->setText($dataArchive[self::UNARCHIVE]);
                } else {
                    $status->setText($dataArchive[self::ARCHIVE])
                        ->addAttributes(['class' => 'text-danger']);
                }

                return $status;
            });

        // FILTER
        $grid->addFilterText('name', 'grid.contact-form.overview.name');
        $grid->addFilterText('emailTo', 'grid.contact-form.overview.email-to');
        $grid->addFilterSelect('isActive', 'grid.contact-form.overview.is-active', $dataArchive)
            ->setPrompt(Constant::PROMTP);

        // ACTION
        $grid->addActionCallback('archive', 'grid.contact-form.overview.action.archive')
            ->setIcon('folder')
            ->setTitle('grid.contact-form.overview.action.archive.title')
            ->setClass('btn btn-xs btn-outline-danger ajax')
            ->onClick[] = function (string $contactFormId) use ($grid): void {
                $contactForm = $this->facade->get(intval($contactFormId));
                $this->facade->archive($contactForm);

                $grid->redrawItem($contactFormId);
            };

        $grid->addActionCallback('unarchive', 'grid.contact-form.overview.action.unarchive')
            ->setIcon('folder-open')
            ->setTitle('grid.contact-form.overview.action.unarchive.title')
            ->setClass('btn btn-xs btn-outline-primary ajax')
            ->onClick[] = function (string $contactFormId) use ($grid): void {
                $contactForm = $this->facade->get(intval($contactFormId));
                $this->facade->unarchive($contactForm);

                $grid->redrawItem($contactFormId);
            };

        if ($this->drawBox) {
            $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());
        }

        // ALLOWED ACTION
        $grid->allowRowsAction('archive', static function (ContactForm $contactForm): bool {
            return $contactForm->isActive();
        });

        $grid->allowRowsAction('unarchive', static function (ContactForm $contactForm): bool {
            return ! $contactForm->isActive();
        });

        // DETAIL
        $detail = $grid->setItemsDetail(__DIR__ . '/detail.latte');
        $detail->setTemplateParameters(['fileStorage' => $this->fileStorage]);

        // OTHER
        $grid->setDefaultSort(['createdAt' => 'DESC']);
        $grid->setDefaultFilter([
            'isActive' => self::UNARCHIVE,
        ]);

        return $grid;
    }
}
