<?php

declare(strict_types=1);

namespace Skadmin\ContactForm;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE = 'contact-form';
    public const DIR_FILE = 'contact-form';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fab fa-fw fa-wpforms']),
            'items'   => ['overview'],
        ]);
    }
}
