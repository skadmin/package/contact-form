<?php

declare(strict_types=1);

namespace Skadmin\ContactForm\Mail;

use DateTimeInterface;
use Haltuf\Genderer\Genderer;
use Skadmin\ContactForm\Doctrine\ContactForm\ContactForm;
use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use SkadminUtils\Utils\Utils\Strings;

use function call_user_func;
use function implode;
use function method_exists;
use function sprintf;

class CMailContactFormCreate extends CMail
{
    public const TYPE = 'contact-form-create';

    private string $title;

    private string $content;

    private string $name;

    private string $vocativName;

    private string $email;

    private string $phone;

    private string $emailTo;

    private string $attachments;

    private DateTimeInterface $createdAt;

    public function __construct(ContactForm $contactForm)
    {
        $genderer = new Genderer();

        $this->title       = $contactForm->getTitle();
        $this->content     = $contactForm->getContent();
        $this->name        = $contactForm->getName();
        $this->vocativName = $genderer->getVocative($contactForm->getName());
        $this->email       = $contactForm->getEmail();
        $this->phone       = $contactForm->getPhone();
        $this->emailTo     = $contactForm->getEmailTo();
        $this->attachments = implode(';', $contactForm->getFiles());
        $this->createdAt   = $contactForm->getCreatedAt();
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize(): array
    {
        $model = [];

        foreach (self::getProperties(self::class) as $property) {
            $description = sprintf('mail.%s.parameter.%s.description', self::TYPE, $property);
            $example     = sprintf('mail.%s.parameter.%s.example', self::TYPE, $property);

            $model[] = (new MailTemplateParameter($property, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues(): array
    {
        $mailParameterValue = [];

        foreach (self::getProperties(self::class) as $property) {
            $method = sprintf('get%s', Strings::camelize($property));

            if (! method_exists($this, $method)) {
                continue;
            }

            $mailParameterValue[] = new MailParameterValue($property, call_user_func([$this, $method]));
        }

        return $mailParameterValue;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): CMailContactFormCreate
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): CMailContactFormCreate
    {
        $this->content = $content;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CMailContactFormCreate
    {
        $this->name = $name;

        return $this;
    }

    public function getVocativName(): string
    {
        return $this->vocativName;
    }

    public function setVocativName(string $vocativName): CMailContactFormCreate
    {
        $this->vocativName = $vocativName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CMailContactFormCreate
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): CMailContactFormCreate
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmailTo(): string
    {
        return $this->emailTo;
    }

    public function setEmailTo(string $emailTo): void
    {
        $this->emailTo = $emailTo;
    }

    public function getAttachments(): string
    {
        return $this->attachments;
    }

    public function setAttachments(string $attachments): void
    {
        $this->attachments = $attachments;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): CMailContactFormCreate
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
